const items = {
  item_392019302: {
    name: "Washing Machine",
    stock: 3,
  },
  item_392019342: {
    name: "Light Bulb",
    stock: 3,
  },
  item_392019340: {
    name: "Streaming Device",
    stock: 2,
  },
  item_392019389: {
    name: "Plug",
    stock: 1,
  },
  item_392019311: {
    name: "Trace",
    stock: 1,
  },
};
// Q1.Form the following solution:
// const result = {
//     washing_machine: {
//         item_id: 'item_392019302',
//         stock: 3
//     },
//     light_bulb: {
//         item_id: 'item_392019342',
//         stock: 3
//     },
//     ...
// }

// Ans1..
let result = Object.keys(items).reduce((acc, curr) => {
  const { name, stock } = items[curr];
  acc[name.toLowerCase().split(" ").join("_")] = {
    item_id: curr,
    stock: stock,
  };
  return acc;
}, {});
console.log(result);

// Q.2 Write a function that takes items object,  propertyName and value as parameters .
// The propertyName is added to each Object.
// Try not to mutate the original object.

// Ans2..
const createObj = (object, propertyName, value) => {
  let output = Object.keys(object).reduce((acc, curr) => {
    const { name, stock } = object[curr];
    acc[curr] = {
      name: name,
      stock: stock,
      [propertyName]: value,
    };
    return acc;
  }, {});
  console.log(output);
};
createObj(items, "input", "output");
// console.log(items)

// Q3. Clone items properly

// Ans3..
// using object.assign
const newItemsUsingAssign = Object.assign(items);
console.log(newItemsUsingAssign);
// using spread operator.
const newItemsUsingSpread = { ...items };
console.log(newItemsUsingSpread);
// console.log(items)
